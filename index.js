async function setupTeam({ config }) {
    console.log("Setting up the team!")
    console.log(config)
    // haah
}

async function processEvent(event, { config, cache }) {
    const counter = await cache.get('counter', 0)
    await cache.set('counter', counter + 1)

    if (event.properties) {
        event.properties['hello'] = 'world'
        event.properties['bar'] = config.bar
        event.properties['$counter'] = counter
        event.properties['lib_number'] = lib_function(3)
    }

    return event
}

function lib_function (number) {
    return number * 2;
}
